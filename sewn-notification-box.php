<?php
/*
Plugin Name: Sewn In Notification Box
Plugin URI: https://bitbucket.org/jupitercow/sewn-in-notification-box
Description: Supports notifications on the front end based off of query variables. Notifications can also be added manually.
Version: 1.0.0
Author: Jake Snyder
Author URI: http://Jupitercow.com/
License: GPLv2
License URI: http://www.gnu.org/licenses/gpl-2.0.html

------------------------------------------------------------------------
Copyright 2013 Jupitercow, Inc.

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

if (! class_exists('sewn_notifications') ) :

add_action( 'init', array('sewn_notifications', 'init') );

class sewn_notifications
{
	/**
	 * Class prefix
	 *
	 * @since 	1.0.0
	 * @var 	string
	 */
	const PREFIX = __CLASS__;

	/**
	 * The default settings
	 *
	 * @since 	1.0.0
	 * @var 	string
	 */
	const VERSION = '1.0.0';

	/**
	 * Settings
	 *
	 * @since 	1.0.0
	 * @var 	string
	 */
	public static $settings = array();

	/**
	 * Holds the current notifications
	 *
	 * @since 	1.0.0
	 * @var 	array
	 */
	private $notifications = array();

	/**
	 * Holds the supported queries and their messages
	 *
	 * @since 	1.0.0
	 * @var 	array
	 */
	private $queries = array();

	/**
	 * Holds the main instance
	 *
	 * @since 	1.0.0
	 * @var 	string
	 */
	private static $_instance = NULL;

	/**
	 * Get instance of class
	 *
	 * Sets up the base, global istance of the class to be used when no specific instances have been created.
	 *
	 * @author  Jake Snyder
	 * @since	1.0.0
	 * @return	object The global instance of this class.
	 */
	public static function get_instance()
	{
		return ( NULL === self::$_instance ) ? self::$_instance == new self : self::$_instance;
	}

	/**
	 * Class construct
	 *
	 * This class can actually be instantiated to create specific notification streams outside of global.
	 *
	 * @author  Jake Snyder
	 * @since	1.0.0
	 * @return	void
	 */
	function __construct()
	{
		// Add an action to show single notifications
		add_action( self::PREFIX . '/show', array($this, 'show_notifications') );

		// Add an action to show single notifications
		add_action( self::PREFIX . '/add', array($this, 'add_notification'), 10, 2 );
	}

	/**
	 * Initialize the Class
	 *
	 * @author  Jake Snyder
	 * @since	1.0.0
	 * @return	void
	 */
	public static function init()
	{
		add_filter( self::PREFIX . '/settings/get_path', array(__CLASS__, 'helpers_get_path'), 1 );
		add_filter( self::PREFIX . '/settings/get_dir',  array(__CLASS__, 'helpers_get_dir'), 1 );

		// Set up settings
		self::$settings = array(
			'path'     => apply_filters( self::PREFIX . '/settings/get_path', __FILE__ ),
			'dir'      => apply_filters( self::PREFIX . '/settings/get_dir',  __FILE__ ),
		);

		// Make sure an instance is created.
		self::get_instance();

		// Dismiss notification
		add_action( 'wp_ajax_' . self::PREFIX . '_dismiss', array(__CLASS__, 'dismiss') );

		// enqueue scripts and styles
		if ( apply_filters( self::PREFIX . '/enqueue_scripts', true ) )
			add_action( 'wp_enqueue_scripts', array(__CLASS__, 'enqueue_scripts') );
	}

	/**
	 * Dismiss persistant notifications permanently
	 *
	 * @author  Jake Snyder
	 * @since	1.0.0
	 * @return	void
	 */
	public static function dismiss()
	{
		if ( empty($_POST['event']) ) return false;

		$current_user = wp_get_current_user();
		update_user_meta( $current_user->ID, self::PREFIX . '_' . esc_sql($_POST['event']) . '_dismissed', current_time('timestamp') );
	}

	/**
	 * Add queries and messages to the instance
	 *
	 * @author  Jake Snyder
	 * @since	1.0.0
	 * @param	string $args Arguments for a new notification(s). The query key/value for request, message for notification, and notification arguments.
	 * @return	void
	 */
	public function add_query( $args )
	{
		$defaults = array(
			'key'     => false,
			'value'   => false,
			'message' => false,
			'args'    => false
		);
		$args = wp_parse_args( $args, $defaults );

		$this->queries[] = $args;
	}

	/**
	 * Get the instance queries and messages, add any global queries and messages
	 *
	 * @author  Jake Snyder
	 * @since	1.0.0
	 * @return	array All of the queries and messages for this instance and globally
	 */
	public function get_queries()
	{
		return apply_filters( self::PREFIX . '/queries', $this->queries );
	}

	/**
	 * Process query vars into notifications
	 *
	 * @author  Jake Snyder
	 * @since	1.0.0
	 * @return	void
	 */
	public function query_notifications()
	{
		$output = array();

		if ( $queries = $this->get_queries() )
		{
			foreach ( $queries as $query )
			{
				if (! empty($query['message']) && ! empty($_REQUEST[$query['key']]) && $query['value'] == $_REQUEST[$query['key']] )
				{
					$output_args = array( 'message' => $query['message'] );
					$output_args['args'] = (! empty($query['args']) ) ? $query['args'] : array();
					$output[] = $output_args;
				}
			}
		}

		return $output;
	}

	/**
	 * Use this to add a notification to the notification area
	 *
	 * @author  Jake Snyder
	 * @since	1.0.0
	 * @param	array|string $notifications The actual text of the notification(s) to add.
	 * @param	array|string $args A collection of arguments to customize the notification. 'dismiss', adds a close button. 'event' applies an event to dismiss and makes it persistent unless it has been closed before. 'fade', fades out the notification after a set time. 'error' makes this an error notification.
	 * @return	void
	 */
	public function add_notification( $message, $args='' )
	{
		$this->notifications[] = array(
			'message' => $message,
			'args'    => $args
		);
	}

	/**
	 * Show all notifications in the notification area
	 *
	 * @author  Jake Snyder
	 * @since	1.0.0
	 * @return	string|null If there are notifications to show, this will return an HTML collection of them.
	 */
	public function get_notifications()
	{
		$notifications = array_merge( $this->notifications, $this->query_notifications() );

		$output = '<div class="' . self::PREFIX . '">';
		foreach ( $notifications as $notification )
		{
			$output .= $this->get_notification( $notification['message'], $notification['args'] );
		}
		$output .= '</div>';

		return $output;
	}

	/**
	 * Get and return a notification
	 *
	 * @author  Jake Snyder
	 * @since	1.0.0
	 * @param	string $message Message for notification
	 * @param	array $args Arguments for notification
	 * @return	string|null If there are notifications to show, this will return an HTML collection of them.
	 */
	public function get_notification( $message, $args )
	{
		$defaults = array(
			'dismiss' => false,
			'event'   => '',
			'fade'    => false,
			'error'   => false,
			'page'    => false
		);
		$args = wp_parse_args( $args, $defaults );
		extract( $args, EXTR_SKIP );

		if ( $page )
		{
			if ( false !== strpos($page, ',') ) $page = explode(',', $page);
			if (! is_page($page) ) return;
		}

		$output = $dismissed = $output_dismiss = '';

		// Set up the dismiss if it is needed
		if ( $dismiss )
		{
			// If event, test if the event has already been dismissed
			$event_data = '';
			if ( $event )
			{
				$current_user = wp_get_current_user();
				$dismissed    = get_user_meta( $current_user->ID, self::PREFIX . '_' . $event . '_dismissed', true );
				$event_data   = ' data-event="'. $event .'"';
			}

			$output_dismiss .= ' <a class="' . self::PREFIX . '-dismiss"' . $event_data . ' href="#dismiss" title="' . __("Dismiss this message", self::PREFIX) . '">';
				$output_dismiss .= __("close", self::PREFIX);
			$output_dismiss .= '</a>';
			$output_dismiss  = apply_filters( self::PREFIX . '/dismiss', $output_dismiss, $args );
		}

		if (! $dismissed )
		{
			if ( 1 == $fade ) $fade = 'true';

			// Set up the notification
			$output  = '<p' . ($error ? ' class="' . self::PREFIX . '-error"' : '') . ($fade ? ' data-fade="' . $fade . '"' : '') . '>';
			$output .= $message;
			$output .= $output_dismiss;
			$output .= '</p>';
		}

		return $output;
	}

	/**
	 * Echo all notifications
	 *
	 * @author  Jake Snyder
	 * @since	1.0.0
	 * @return	void
	 */
	public function show_notifications()
	{
		echo $this->get_notifications();
	}

	/**
	 * Get the plugin path
	 *
	 * Calculates the path (works for plugin / theme folders). These functions are from Elliot Condon's ACF plugin.
	 *
	 * @since	0.1
	 * @return	void
	 */
	public static function helpers_get_path( $file )
	{
	   return trailingslashit( dirname($file) );
	}

	/**
	 * Get the plugin directory
	 *
	 * Calculates the directory (works for plugin / theme folders). These functions are from Elliot Condon's ACF plugin.
	 *
	 * @since	0.1
	 * @return	void
	 */
	public static function helpers_get_dir( $file )
	{
        $dir = trailingslashit( dirname($file) );
        $count = 0;

        // sanitize for Win32 installs
        $dir = str_replace('\\' ,'/', $dir);

        // if file is in plugins folder
        $wp_plugin_dir = str_replace('\\' ,'/', WP_PLUGIN_DIR); 
        $dir = str_replace($wp_plugin_dir, plugins_url(), $dir, $count);

        if ( $count < 1 )
        {
	       // if file is in wp-content folder
	       $wp_content_dir = str_replace('\\' ,'/', WP_CONTENT_DIR); 
	       $dir = str_replace($wp_content_dir, content_url(), $dir, $count);
        }

        if ( $count < 1 )
        {
	       // if file is in ??? folder
	       $wp_dir = str_replace('\\' ,'/', ABSPATH); 
	       $dir = str_replace($wp_dir, site_url('/'), $dir);
        }

        return $dir;
    }

	/**
	 * Enqueue scripts and styles
	 *
	 * @author  Jake Snyder
	 * @since	1.0.0
	 * @return	void
	 */
	public static function enqueue_scripts()
	{
		// register acf scripts
		wp_register_script( self::PREFIX, self::$settings['dir'] . 'js/scripts.js', array( 'jquery' ), self::VERSION );
		$args = array(
			'url'    => admin_url( 'admin-ajax.php' ),
			'action' => self::PREFIX . '_dismiss',
			'nonce'  => wp_create_nonce( self::PREFIX . '_dismiss' )
		);
		wp_localize_script( self::PREFIX, 'frontnotify', $args );

		wp_enqueue_script( array(
			self::PREFIX
		) );

		// styles
		wp_register_style( self::PREFIX, self::$settings['dir'] . 'css/style.css', array(), self::VERSION );

		wp_enqueue_style( array(
			self::PREFIX
		) );
	}
}

endif;